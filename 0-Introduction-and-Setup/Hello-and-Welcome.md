Hello and Welcome to this course
# Container Security with Kubernetes & GitLab

![Hello and Welcome Slide](img/00-SLIDES-Container-Security-with-Kubernetes-and-Gitlab_HELLO-AND-WELCOME_05092024.png)

## What this course will teach you
- Security first implementation of CI/CD pipeline with a simple app
- Build containers securely
- Scan images for vulnerabilities
- Host your own GitLab server
- Use GitLab CI/CD to deploy to a Kubernetes environment
- Use the Kubernetes executor to run CI/CD jobs in GitLab
- Host your own container registry in GitLab
- Add elements to your pipeline to secure your application

## About the Instructor
- [Chad M. Crowell](https://chadcrowell.com)
- [Chad's book](https://acingthecka.com)
- [Chad's other courses](https://community.kubeskills.com/courses)
- [Kubeskills Community](https://kubeskills.com)


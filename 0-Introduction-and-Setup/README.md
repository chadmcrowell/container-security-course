# Container Security with Kubernetes & GitLab CI/CD

![Container Security Course Title Slide](img/00-SLIDES-Container-Security-with-Kubernetes-and-Gitlab_HELLO-AND-WELCOME_05092024.png)

## SECTION 0: Introduction and Setup

- LESSON 0: [Hello and Welcome](Hello-and-Welcome.md)
- LESSON 1: [Course Requirements](Course-requirements.md)
- LESSON 2: [Quick GitLab Walkthrough](Quick-GitLab-Walkthrough.md)
- LESSON 3: [Installing a Self-Managed GitLab Server](Installing-GitLab-Server.md)
- LESSON 4: [Quick Kubernetes Walkthrough](Quick-K8s-walkthrough.md)
- LESSON 5: [Installing Kubernetes](Installing-Kubernetes.md)
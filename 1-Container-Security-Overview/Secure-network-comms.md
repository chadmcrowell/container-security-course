# Secure Network Communication to and from Containers

![Secure Network Communications title slide](img/09-SLIDES-Container-Security-with-Kubernetes-and-Gitlab_SECURE-NET-COMMS_06172024.001.png)

## Container Network
- iptables rules
- Network Policies
- Intrusion Detection
- Proxy Traffic

## What we get with Kubernetes:
- **NETPOL** - traffic isolation, and segmenting traffic: https://kubernetes.io/docs/concepts/services-networking/network-policies/
- **INGRESS** - SSL/TLS termination to secure communications, depending on the ingress controller, you can use, you can achieve rate limiting to protect against DDoS attacks. https://kubernetes.io/docs/concepts/services-networking/ingress/
- **DNS** - don't have to hardcode IPs, DNSSEC to protect against DNS spoofing attacks. https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/
- CNI - Enable advanced networking to enforce security policies. https://github.com/containernetworking/cni

[Example of applying deny-all network policy](https://github.com/ahmetb/kubernetes-network-policy-recipes/blob/master/01-deny-all-traffic-to-an-application.md)

[kyverno example](https://www.youtube.com/live/ka0C09CAfho?si=0ZfXVXnB-gUdeLbJ&t=8147)
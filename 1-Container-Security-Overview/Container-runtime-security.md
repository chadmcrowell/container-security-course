# Container Runtime Security

![Container Runtime Security Title Slide](img/08-SLIDES-Container-Security-with-Kubernetes-and-Gitlab_CONTAINER-RUNTIME-SECURITY_06142024.001.png)

## Container-runtime
- **ROOTLESS CONTAINERS:** 
	- completely without root privileges to prevent escalation attacks.
- **KERNEL HARDENING:** 
	- SELinux/AppArmor: Enforce security policies (e.g. web server can't access files in a user's home dir)
	- Seccomp: to restrict system calls to the kernel
- **IMAGE SECURITY:** Only use trusted images, and scan images for vuln regularly with Clair or Trivy
- **FILESYSTEM ACCESS:** In addition to using read-only filesystem, Use Falco to detect abnormal behavior (e.g. shell has spawned inside a container).

## What we get with Kubernetes:
- Pod Security Standards (PSS): set contstraints on the security context of pods, like privilege escalation, capabilities and allowed volumes. https://kubernetes.io/docs/concepts/security/pod-security-standards/
- Secrets Management (secrets and integrations with key management services (KMS))
	- Avoid embedding sensitive information in images by using **ConfigMaps and Secrets**
	- Use ImagePullSecrets - pull images from private container registries by storing credentials in K8s Secrets
- **Image Policy Webhook**: An admission controller that can enforce policies around the images used in the cluster, such as requiring signed images.
- **Seccomp Profiles** (https://kubernetes.io/docs/tutorials/security/seccomp/): Kubernetes supports the use of Seccomp profiles to restrict the system calls that containers can make, enhancing security by reducing the attack surface.
- **AppArmor and SELinux**: These Linux security modules can be used to enforce mandatory access control policies on containers running within Kubernetes.

## Container Security Tools
- VULN SCANS: scanning images and code for vulnerabilities.
- RUNTIME PROTECT: runtime protection including enforcing security policies and detecting anomolies.
- IMAGE VERIFY: Ensure that images have not been tampered with by verifying digital signatures
- BENCHMARK: Cross-check security configuration against security benchmarks


- **TRIVY:** Scans container images, file systems, and Git repositories for vulnerabilities
- **TWISTLOCK:** vulnerability management, runtime protection, compliance enforcement for containers
- **SysDig Secure:** offers vulnerability management, runtime protection, compliance enforcement, and incident response.
- **GRSECURITY/PAX:** provides access control, memory protection, and kernel hardening.
- **FALCO:** detects and alerts on anomalous behavior in containerized environments by monitoring system calls
- **Kube-bench:** Checks the security configuration of Kubernetes clusters against the CIS Kubernetes Benchmark
- **Kube-hunter:** Identifies vulnerabilities and misconfigurations in Kubernetes deployments.
- **Anchore Engine:** Scans images for known vulnerabilities, secrets, configuration issues, and provides detailed reports
- **Notary:** Ensures that images have not been tampered with by verifying digital signatures before deployment
- **OpenSCAP:** Provides tools for vulnerability scanning, compliance checking, and policy enforcement.
- **StackRox:** Includes vulnerability management, compliance, configuration management, and runtime security.
- **Snyk:** Finds and fixes vulnerabilities in your code, dependencies, containers, and configurations.
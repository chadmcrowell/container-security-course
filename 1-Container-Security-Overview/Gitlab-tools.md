# GitLab Tools for Container Security

![GitLab Tools for Security title slide](img/10-SLIDES-Container-Security-with-Kubernetes-and-Gitlab_GITLAB-TOOLS-FOR-SEC_06282024.001.png)

👇Top 10 From here: https://owasp.org/www-project-top-10-ci-cd-security-risks/
**The Open Worldwide Application Security Project (OWASP)**

## Top 10 CI/CD Security Risks
1. **Insufficient Flow Control Mechanisms:** ability for an attacker to push malicious code or artifacts down the pipeline (no enforcement or approval)
2. **Inadequate Identity and Access Management:** lack of control over identities - both human and programatic accounts - increases the attack vectors
3. **Dependency Chain Abuse:** flaws related to how build systems fetch code dependencies, which leads to malicious packages being fetched.
4. **Poisoned Pipeline Execution (PPE):** an attacker injecting malicious code/commands into the pipeline configuration due to poor access controls on version control systems (i.e. GitLab).
5. **Insufficient PBAC (Pipeline-Based Access Controls):** Access to the pipeline execution nodes are compromised, leading to running malicious code run within a pipeline step. (e.g. accessing CI/CD env variables, AWS metadata service, etc.)
6. **Insufficient Credential Hygiene:** The attacker's ability to use secrets, tokens, etc. that are spread throughout the pipeline code and on runners (printed to console, or unrotated creds)
7. **Insecure System Configuration:** Flaws in security settings, configuration, and hardening of systems across the entire pipeline.
8. **Ungoverned Usage of 3rd Party Services:** situations in which 3rd party services have access to code or pipeline resources, granting too much access. 
9. **Improper Artifact Integrity Validation:** An attacker with access to the pipeline pushes malicious code or artifacts down the pipeline. 
10. **Insufficient Logging and Visibility:** Attacker can carry out malicious activities in the CI/CD environment without detection.

## GitLab Insufficient Flow Control Protection
GitLab employs various mechanisms to protect against insufficient flow control, ensuring that the CI/CD pipeline processes and other workflows are executed securely and efficiently. Below are some of the key protections and controls GitLab has in place:
#### 1. **Role-Based Access Control (RBAC)**
GitLab uses RBAC to ensure that only authorized users can perform certain actions:
- **Permissions**: Different roles (e.g., Guest, Reporter, Developer, Maintainer, Owner) have different permissions, which restrict access to various features and settings within a project or group.
- **Protected Branches**: You can define protected branches to restrict who can push to or merge into these branches. This prevents unauthorized changes to critical parts of your codebase.
#### 2. **Pipeline Permissions**
- **Job Permissions**: Jobs in GitLab CI/CD pipelines inherit the permissions of the user who triggered the pipeline, ensuring that unauthorized users cannot execute sensitive jobs.
- **Protected Variables**: GitLab allows you to protect CI/CD variables, ensuring they are only available to jobs running on protected branches or tags.
#### 3. **Pipeline and Job Configuration**
- **YAML Configuration**: The `.gitlab-ci.yml` file, which defines the pipeline, can be configured to include conditions that control the flow of jobs based on specific rules and triggers. This ensures that jobs are only run when certain conditions are met.
- **Job Dependencies**: Define job dependencies explicitly in the pipeline configuration to ensure jobs run in the correct order and only when their prerequisites are complete.
#### 4. **Security Scans and Policies**
- **Static Application Security Testing (SAST)**: GitLab can automatically scan your code for security vulnerabilities as part of the CI/CD pipeline.
- **Dependency Scanning**: Identify vulnerabilities in the dependencies your projects rely on.
- **Container Scanning**: Scan Docker images for vulnerabilities before deploying them.
#### 5. **Two-Factor Authentication (2FA)**
GitLab supports 2FA to add an extra layer of security to user accounts, reducing the risk of unauthorized access.
#### 6. **Audit Logs**
- **Logging and Monitoring**: GitLab provides audit logs that record important actions performed by users, such as changes to project settings, pipeline executions, and more. These logs help in monitoring and investigating security-related incidents.
#### 7. **Pipeline Execution Control**
- **Pipeline Schedules**: Schedule pipelines to run at specific times, reducing the risk of resource exhaustion due to uncontrolled pipeline executions.
- **Manual Jobs**: Include manual jobs in your pipeline that require a user to trigger them explicitly, providing control over when certain actions are performed.
#### 8. **Rate Limiting and Throttling**
- **Rate Limiting**: GitLab employs rate limiting to protect against abuse and ensure fair usage of resources. This prevents any single user from overwhelming the system with too many requests or jobs.
- **Throttling**: Throttling mechanisms are in place to manage the number of concurrent jobs and pipeline executions, ensuring that resources are used efficiently and preventing bottlenecks.
#### 9. **Container Registry and Package Registry**
- **Access Control**: GitLab's Container and Package Registries have access controls to ensure that only authorized users can push, pull, and manage container images and packages.
#### 10. **Environment and Secret Management**
- **Secret Variables**: GitLab CI/CD supports secret variables that can be securely stored and used within pipelines without exposing them in the pipeline configuration or logs.
- **Environment Protection**: Define environments in GitLab (e.g., staging, production) and protect them by restricting who can deploy to these environments.
#### 11. **Dynamic Application Security Testing (DAST)**
- **DAST**: GitLab can perform DAST to identify runtime vulnerabilities in your web applications by simulating external attacks during the CI/CD pipeline execution.

## GitLab Inadequate Identity and Access Management Protection
GitLab implements a comprehensive suite of features and best practices to protect against inadequate identity and access management (IAM). These measures ensure that only authorized users can access and perform actions within GitLab projects, repositories, and pipelines. Below are the key protections GitLab employs:
#### 1. **Role-Based Access Control (RBAC)**
- **Roles and Permissions**: GitLab uses RBAC to assign specific roles to users (e.g., Guest, Reporter, Developer, Maintainer, Owner) with defined permissions. Each role has a distinct set of capabilities, restricting access to sensitive operations based on the user's role.
- **Granular Permissions**: Permissions can be set at the group, project, and branch levels, providing fine-grained control over who can perform specific actions.
#### 2. **Two-Factor Authentication (2FA)**
- **Mandatory 2FA**: Administrators can enforce mandatory 2FA for all users, adding an additional layer of security to user accounts.
- **2FA Enforcement Policies**: GitLab allows the configuration of 2FA enforcement policies, ensuring that users cannot bypass this requirement.
#### 3. **Single Sign-On (SSO) and SAML Integration**
- **SSO Support**: GitLab supports SSO integration with providers such as Okta, OneLogin, and Microsoft Azure AD, allowing users to authenticate using their corporate credentials.
- **SAML Integration**: GitLab integrates with SAML-based identity providers for seamless and secure user authentication.
#### 4. **LDAP Integration**
- **LDAP Authentication**: GitLab supports LDAP integration, enabling organizations to manage user identities and access through their existing LDAP directory services.
- **User Synchronization**: LDAP user synchronization ensures that user data is consistently updated and managed centrally.
#### 5. **OAuth2 and OpenID Connect (OIDC)**
- **OAuth2**: GitLab supports OAuth2 for secure third-party application access.
- **OIDC**: GitLab can act as an OpenID Connect provider, allowing secure authentication and authorization for applications.
#### 6. **Personal Access Tokens and Deploy Tokens**
- **Personal Access Tokens**: Users can generate personal access tokens with specific scopes and expiration dates for secure API access.
- **Deploy Tokens**: Deploy tokens provide read-only or read-write access to repositories for CI/CD pipelines and deployment purposes, with controlled scope and duration.
#### 7. **SSH Key Management**
- **SSH Key Authentication**: GitLab supports SSH key authentication for secure access to repositories.
- **Key Rotation Policies**: Administrators can enforce SSH key rotation policies to ensure keys are regularly updated and invalidated when no longer needed.
#### 8. **Audit Logs**
- **Comprehensive Logging**: GitLab maintains detailed audit logs of user activities, including login attempts, changes to settings, and access to repositories.
- **Monitoring and Alerts**: Administrators can monitor audit logs for suspicious activities and configure alerts for critical events.
#### 9. **Protected Branches and Tags**
- **Branch Protection**: GitLab allows the protection of branches and tags, restricting who can push or merge changes. This ensures that only authorized users can make changes to critical parts of the codebase.
- **Merge Request Approvals**: Configure required approvals for merge requests to ensure changes are reviewed by multiple stakeholders before being integrated.
#### 10. **Group and Project Management**
- **Nested Groups**: GitLab supports nested groups, allowing for hierarchical management of permissions and access.
- **Project Visibility**: Set project visibility to private, internal, or public to control access levels.
#### 11. **Environment and Secret Management**
- **Environment-Specific Access**: Control access to different environments (e.g., development, staging, production) to ensure that only authorized users can deploy to sensitive environments.
- **Protected Variables**: Securely manage and restrict access to environment variables and secrets used in CI/CD pipelines.
#### 12. **IP Allowlisting**
- **IP Restrictions**: Administrators can configure IP allowlists to restrict access to GitLab to specified IP addresses, enhancing security by limiting access to trusted networks.
#### 13. **Session Management**
- **Session Timeouts**: Configure session timeout settings to automatically log out inactive users, reducing the risk of unauthorized access.
- **Session Expiry Policies**: Implement session expiry policies to ensure users re-authenticate periodically.
#### 14. **User and Group Management**
- **User Deactivation**: Administrators can deactivate user accounts that are no longer needed, immediately revoking access.
- **Group Membership Control**: Manage group memberships to ensure users have appropriate access levels and are removed when no longer required.
#### 15. **Security Policies and Compliance**
- **Compliance Frameworks**: GitLab supports compliance with various frameworks such as GDPR, HIPAA, and SOC 2, providing features to help meet regulatory requirements.
- **Security Policies**: Define and enforce security policies to manage user access and behavior within GitLab.

## GitLab Dependency Chain Abuse Protection
GitLab implements several strategies and features to protect against dependency chain abuse, ensuring that dependencies used in projects are secure and trustworthy. Here are the key protections and mechanisms GitLab employs:
#### 1. ***Dependency Scanning***
- ***Automated Scans**: GitLab CI/CD can automatically scan for known vulnerabilities in project dependencies using Dependency Scanning. This feature checks for security issues in the dependencies specified in the project's package management files (e.g., `Gemfile`, `package.json`, `pom.xml`).*
- ***Integration with CI/CD**: Dependency scanning is integrated into the CI/CD pipeline, providing immediate feedback on the security status of dependencies whenever code is pushed or merged.*
#### 2. Security Dashboard
- **Project Security Dashboard**: Displays a summary of security vulnerabilities detected in the project's dependencies, allowing developers to quickly identify and address issues.
- **Group Security Dashboard**: Provides an aggregated view of security vulnerabilities across multiple projects within a group, helping to manage and prioritize security efforts at a higher level.
#### 3. Vulnerability Management
- **Vulnerability Reports**: Detailed reports on detected vulnerabilities, including severity, affected components, and recommendations for remediation.
- **Issue Tracking**: Automatically create issues for detected vulnerabilities, integrating security tasks into the existing project management workflow.
#### 4. Dependency Update Automation
- **Dependency Bots**: GitLab can integrate with dependency update tools (e.g., Dependabot) that automatically check for and update dependencies to the latest secure versions. This helps ensure that dependencies are regularly updated to versions without known vulnerabilities.
#### 5. ***Package and Container Registry Security***
- ***Package Registry**: GitLab's integrated package registry allows for secure storage and management of project dependencies, ensuring that only trusted packages are used.*
- ***Container Scanning**: Scans Docker images for known vulnerabilities, ensuring that the base images and layers used in containers are secure.*
#### 6. Third-Party Dependency Policies
- **Approval Workflows**: Implement workflows that require approvals for adding or updating dependencies, ensuring that new dependencies are reviewed for security risks before being included in the project.
- **License Compliance**: GitLab's license compliance feature scans dependencies for license compatibility, helping to avoid legal risks associated with using dependencies that have restrictive or incompatible licenses.
#### 7. Security Alerts and Notifications
- **Alerts**: Developers and maintainers receive alerts when vulnerabilities are detected in dependencies, enabling prompt action to address the issues.
- **Email Notifications**: Configurable email notifications to keep team members informed about new vulnerabilities and remediation status.
#### 8. Integration with External Security Tools
- **Security Tool Integration**: GitLab supports integration with external security tools and services that provide additional scanning and monitoring capabilities, enhancing the security coverage for dependencies.
#### 9. ***Enforcing Dependency Policies***
- ***Dependency Allowlists and Blocklists**: Maintain lists of approved and banned dependencies, ensuring that only trusted and reviewed packages are used in the project.*
- ***Custom Scanning Rules**: Define custom scanning rules and policies to enforce specific security requirements and standards for dependencies.*
#### 10. Continuous Monitoring and Auditing
- **Continuous Monitoring**: Ongoing monitoring of dependencies for newly discovered vulnerabilities, ensuring that projects remain secure over time.
- **Audit Logs**: Maintain logs of changes to dependencies, providing an audit trail for dependency management and helping to identify and respond to suspicious activity.
#### 11. Secure Development Practices
- **Education and Training**: Promote secure development practices among developers, including the importance of using secure dependencies and regularly updating them.
- **Code Reviews**: Include dependency checks as part of the code review process to ensure that new dependencies are evaluated for security risks.
#### Implementing Dependency Scanning in GitLab CI/CD
Here is an example of how you can implement dependency scanning in your GitLab CI/CD pipeline:
```yaml
stages:
  - build
  - test
  - dependency-scanning
  - deploy

# Dependency Scanning stage
dependency-scanning:
  stage: dependency-scanning
  image: docker:latest
  services:
    - docker:dind
  script:
    - apt-get update && apt-get install -y python3-pip
    - pip3 install gitlab-ci-pipeline-utilities
    - dependency-scanning scan
  artifacts:
    reports:
      dependency_scanning: dependency-scanning-report.json
  only:
    - main
```
#### Explanation
- **Stages**: Defines different stages in the pipeline, including `dependency-scanning`.
- **Dependency Scanning**: Runs dependency scanning using a specific image and script. The scanning results are stored as artifacts and can be viewed in the GitLab interface.


## GitLab Poisoned Pipeline Execution (PPE) Protection

GitLab employs various security mechanisms and best practices to protect against poisoned pipeline execution, ensuring that only trusted and verified code and dependencies are executed within the CI/CD pipeline. Here are the key protections GitLab employs:
#### 1. ***Pipeline Security Policies***
- ***Approval Workflows**: GitLab allows the configuration of approval workflows for pipeline execution, requiring certain jobs or entire pipelines to be manually approved by authorized users before they run.*
- ***Protected Branches and Tags**: GitLab supports protected branches and tags, ensuring that only authorized users can push changes or trigger pipelines on these branches. This helps prevent unauthorized or malicious code from being executed.*
#### 2. Role-Based Access Control (RBAC)
- **Granular Permissions**: GitLab's RBAC system assigns specific roles to users (e.g., Guest, Reporter, Developer, Maintainer, Owner) with defined permissions. This ensures that only users with appropriate permissions can modify pipeline configurations or execute pipelines.
- **Least Privilege Principle**: GitLab encourages the principle of least privilege by allowing fine-grained control over user permissions, reducing the risk of unauthorized access and execution.
#### 3. ***CI/CD Variables and Secrets Management***
- ***Protected Variables**: CI/CD variables can be protected, making them accessible only in pipelines running on protected branches or tags. This prevents unauthorized access to sensitive information such as credentials and API keys.*
- ***Secret Management**: GitLab supports the secure storage and management of secrets used in pipelines, ensuring they are not exposed in logs or scripts.*
#### 4. ***Static and Dynamic Code Analysis***
- ***Static Application Security Testing (SAST)**: GitLab can perform SAST to detect security vulnerabilities in the source code before it is merged and executed.*
- ***Dynamic Application Security Testing (DAST)**: DAST scans can identify vulnerabilities in running applications, ensuring that the deployed code does not contain exploitable issues.*
#### 5. Dependency Scanning
- **Automated Scans**: GitLab's dependency scanning feature checks for known vulnerabilities in project dependencies, preventing poisoned dependencies from being used in pipelines.
- **Security Dashboards**: Security dashboards provide visibility into the vulnerabilities detected in dependencies, helping teams to prioritize and address them promptly.
#### 6. Container Scanning and Hardening
- **Container Scanning**: Scans Docker images for known vulnerabilities, ensuring that the images used in pipelines are secure.
- **Container Hardening**: Best practices for container hardening are encouraged, such as using minimal base images and applying security patches regularly.
#### 7. Pipeline Configuration and Execution Controls
- **YAML Configuration**: The `.gitlab-ci.yml` file defines pipeline configurations with strict controls over job execution, dependencies, and triggers.
- **Job Dependencies and Rules**: Explicitly define job dependencies and rules to ensure that jobs run in a controlled and predictable manner.
- **Manual Jobs**: Include manual jobs that require user intervention to execute, adding an additional layer of control over critical actions.
#### 8. Secure Runners and Execution Environments
- **Runner Isolation**: GitLab CI/CD runners can be configured to run in isolated environments, reducing the risk of cross-job contamination and unauthorized access.
- **Trusted Runners**: Use only trusted and verified runners to execute pipelines, ensuring that the execution environment is secure.
#### 9. Audit Logs and Monitoring
- **Comprehensive Logging**: GitLab maintains detailed audit logs of user activities, pipeline executions, and configuration changes, providing an audit trail for security investigations.
- **Monitoring and Alerts**: Administrators can monitor audit logs for suspicious activities and configure alerts for critical events, enabling timely response to potential security incidents.
#### 10. Security Training and Best Practices
- **Developer Training**: Educate developers on secure coding practices and the importance of maintaining a secure CI/CD pipeline.
- **Security Best Practices**: Encourage adherence to security best practices, such as code reviews, automated testing, and regular security assessments.
#### Example of a Secure GitLab CI/CD Pipeline Configuration
Here is an example of how to implement some of these security practices in a `.gitlab-ci.yml` file:

```yaml
stages:
  - build
  - test
  - security
  - deploy

variables:
  IMAGE_TAG: "latest"
  SECURE_VARIABLE: $CI_JOB_TOKEN

# Build Stage
build:
  stage: build
  script:
    - docker build -t $CI_REGISTRY_IMAGE:$IMAGE_TAG .
  only:
    - protected-branch

# Test Stage
test:
  stage: test
  script:
    - echo "Running tests..."
  only:
    - protected-branch

# Security Stage
dependency-scanning:
  stage: security
  script:
    - dependency-scanning scan
  artifacts:
    reports:
      dependency_scanning: dependency-scanning-report.json
  only:
    - protected-branch

# Deploy Stage
deploy:
  stage: deploy
  script:
    - docker run -d -p 80:80 $CI_REGISTRY_IMAGE:$IMAGE_TAG
  only:
    - protected-branch
  environment:
    name: production
    url: https://my-production-url.com
  when: manual
  allow_failure: false

# Secure Variables and Secrets
secrets:
  stage: test
  script:
    - echo "Using secret variable: $SECURE_VARIABLE"
  only:
    - protected-branch
```

#### Explanation
- **Stages**: Defines stages (build, test, security, deploy) in the pipeline.
- **Protected Branches**: Ensures that jobs are only executed on protected branches.
- **Manual Deployment**: The deploy job requires manual approval, adding a layer of control over the deployment process.
- **Dependency Scanning**: Includes a security stage for dependency scanning, ensuring vulnerabilities in dependencies are detected and reported.
- **Secure Variables**: Demonstrates the use of a secure variable, ensuring sensitive information is protected.


## GitLab Insufficient Pipeline-Based Access Controls (PBAC) Protection

GitLab employs several strategies to protect against insufficient pipeline-based access controls, ensuring that pipelines and their associated resources are secured from unauthorized access and misuse. Below are the key protections and mechanisms GitLab uses:
#### 1. Role-Based Access Control (RBAC)
- **Granular Permissions**: GitLab provides granular permissions for different roles, such as Guest, Reporter, Developer, Maintainer, and Owner. Each role has specific capabilities, restricting what users can do in projects and pipelines.
- **Custom Role Definitions**: Admins can define custom roles and permissions, tailoring access control to the specific needs of the organization.
#### 2. Protected Branches and Tags
- **Branch Protection**: GitLab allows you to protect branches, preventing unauthorized users from pushing changes directly. Only users with sufficient permissions can push to or merge into protected branches.
- **Tag Protection**: Similar to branch protection, you can protect tags to ensure that only authorized users can create or delete them.
#### 3. ***Pipeline Configuration and Execution Controls***
- ***Pipeline Visibility**: Control the visibility of pipelines to ensure that only authorized users can view or trigger them.*
- ***Job Permissions**: Define specific permissions for pipeline jobs, ensuring they only execute in the correct contexts and with the right permissions.*
- ***Manual Jobs**: Include manual jobs that require explicit user approval to execute, adding an additional layer of control for critical actions.*
#### 4. CI/CD Variables and Secret Management
- **Protected Variables**: CI/CD variables can be marked as protected, making them accessible only to jobs running on protected branches or tags. This prevents unauthorized access to sensitive information.
- **Masked Variables**: Variables can be masked to hide their values in job logs, preventing exposure of sensitive data.
#### 5. **Approval Workflows**
- **Merge Request Approvals**: Configure merge request approval rules to require reviews from multiple team members before code can be merged. This ensures that changes are reviewed by authorized personnel.
- **Job Approvals**: Certain jobs in a pipeline can require manual approval before execution, ensuring that only approved changes are deployed or executed.
#### 6. **Audit Logs and Monitoring**
- **Comprehensive Logging**: GitLab maintains detailed audit logs of user activities, pipeline executions, and configuration changes, providing a trail for security audits and investigations.
- **Monitoring and Alerts**: Administrators can set up monitoring and alerts for unusual activities, enabling timely detection and response to potential security incidents.
#### 7. **Pipeline Protection and Restrictions**
- **Protected Environments**: Define protected environments (e.g., staging, production) and restrict who can deploy to these environments. This ensures that only authorized users can promote code to critical environments.
- **Environment-Specific Access**: Control access to different environments based on user roles and permissions.
#### 8. **Two-Factor Authentication (2FA)**
- **Mandatory 2FA**: Administrators can enforce mandatory 2FA for all users, adding an extra layer of security to user accounts and reducing the risk of unauthorized access.
#### 9. **IP Allowlisting**
- **IP Restrictions**: Implement IP allowlisting to restrict access to GitLab from specified IP addresses, enhancing security by limiting access to trusted networks.
#### 10. **External Authentication and SSO**
- **SSO Integration**: GitLab supports Single Sign-On (SSO) integration with identity providers such as Okta, OneLogin, and Azure AD, ensuring secure and centralized user authentication.
- **LDAP Integration**: GitLab can integrate with LDAP for user authentication and management, providing centralized control over user access.
#### Example of a Secure GitLab CI/CD Pipeline Configuration
Here is an example of how to implement these security practices in a `.gitlab-ci.yml` file:
```yaml
stages:
  - build
  - test
  - deploy

variables:
  IMAGE_TAG: "latest"
  SECURE_VARIABLE: $CI_JOB_TOKEN

# Build Stage
build:
  stage: build
  script:
    - docker build -t $CI_REGISTRY_IMAGE:$IMAGE_TAG .
  only:
    - protected-branch

# Test Stage
test:
  stage: test
  script:
    - echo "Running tests..."
  only:
    - protected-branch

# Deploy Stage
deploy:
  stage: deploy
  script:
    - docker run -d -p 80:80 $CI_REGISTRY_IMAGE:$IMAGE_TAG
  only:
    - protected-branch
  environment:
    name: production
    url: https://my-production-url.com
  when: manual
  allow_failure: false

# Secure Variables and Secrets
secrets:
  stage: test
  script:
    - echo "Using secret variable: $SECURE_VARIABLE"
  only:
    - protected-branch
```
#### Explanation
- **Stages**: Defines stages (build, test, deploy) in the pipeline.
- **Protected Branches**: Ensures that jobs are only executed on protected branches.
- **Manual Deployment**: The deploy job requires manual approval, adding a layer of control over the deployment process.
- **Protected Variables**: Demonstrates the use of protected variables, ensuring sensitive information is protected.


## GitLab Insufficient Credential Hygiene Protection
GitLab employs various strategies and features to protect against insufficient credential hygiene, ensuring that credentials are managed securely and reducing the risk of unauthorized access. Here are the key protections and mechanisms GitLab uses:
#### 1. Secret Management
- **CI/CD Variables**: GitLab CI/CD allows you to define environment variables that can be masked and protected. Masked variables do not appear in job logs, and protected variables are only available in pipelines running on protected branches or tags.
- **Secret Detection**: GitLab can scan code repositories for sensitive data such as API keys, passwords, and tokens to prevent accidental exposure of credentials in the codebase.
#### 2. Two-Factor Authentication (2FA)
- **Mandatory 2FA**: Administrators can enforce 2FA for all users, adding an extra layer of security to user accounts. This ensures that even if credentials are compromised, unauthorized access is less likely.
- **Enforced 2FA Policies**: GitLab supports policies that require users to enable 2FA before they can access certain resources or perform specific actions.
#### 3. ***Personal Access Tokens***
- ***Scoped Tokens**: Personal access tokens can be created with specific scopes and expiration dates, limiting their permissions and reducing the risk of misuse.*
- ***Token Management**: Users can view and revoke their tokens through the GitLab UI, allowing them to maintain control over their credentials.*
#### 4. ***SSH Key Management***
- ***SSH Key Rotation**: GitLab supports SSH key rotation, allowing users to regularly update their keys and remove old or compromised keys.*
- ***Key Restrictions**: Administrators can enforce policies on the types and strengths of SSH keys that users can register, ensuring that only secure keys are used.*
#### 5. **Audit Logs and Monitoring**
- **Comprehensive Logging**: GitLab maintains detailed audit logs of user activities, including authentication attempts, changes to credentials, and access to repositories. These logs help in identifying and investigating suspicious activities.
- **Monitoring and Alerts**: Administrators can set up monitoring and alerts for unusual activities related to credential usage, enabling timely detection and response to potential security incidents.
#### 6. **Single Sign-On (SSO) and External Authentication**
- **SSO Integration**: GitLab supports SSO with providers like Okta, OneLogin, and Azure AD, allowing users to authenticate using their corporate credentials. This centralizes credential management and reduces the risk of credential misuse.
- **LDAP Integration**: GitLab can integrate with LDAP for user authentication, enabling centralized management of user credentials and policies.
#### 7. **IP Allowlisting**
- **IP Restrictions**: GitLab supports IP allowlisting, which restricts access to the GitLab instance or specific resources to trusted IP addresses. This reduces the risk of unauthorized access from unknown or suspicious locations.
#### 8. **Dependency Management**
- **Secure Dependencies**: GitLab's dependency scanning feature helps identify vulnerabilities in project dependencies, ensuring that third-party libraries and tools do not introduce security risks, including those related to credential exposure.
#### 9. **Enforcing Credential Policies**
- **Password Policies**: GitLab administrators can enforce password complexity and expiration policies, ensuring that user passwords meet security standards.
- **Credential Storage**: GitLab stores credentials securely using encryption, reducing the risk of credential theft through database breaches.
#### 10. **Automated Credential Rotation**
- **Credential Rotation**: Regularly rotating credentials, such as API keys and tokens, can minimize the risk of long-term exposure. Automated tools and scripts can be used to rotate credentials and update them in GitLab securely.
#### 11. **Environment-Specific Secrets**
- **Environment Variables**: Define environment-specific variables and secrets to ensure that credentials used in different environments (e.g., development, staging, production) are isolated and managed securely.
#### Example of Secure GitLab CI/CD Pipeline Configuration
Here is an example of a `.gitlab-ci.yml` configuration that demonstrates secure handling of credentials:
```yaml
stages:
  - build
  - test
  - deploy

variables:
  IMAGE_TAG: "latest"
  SECURE_VARIABLE: $CI_JOB_TOKEN

# Build Stage
build:
  stage: build
  script:
    - docker build -t $CI_REGISTRY_IMAGE:$IMAGE_TAG .
  only:
    - protected-branch
  variables:
    DOCKER_AUTH: $DOCKER_AUTH_TOKEN

# Test Stage
test:
  stage: test
  script:
    - echo "Running tests..."
  only:
    - protected-branch

# Deploy Stage
deploy:
  stage: deploy
  script:
    - docker run -d -p 80:80 $CI_REGISTRY_IMAGE:$IMAGE_TAG
  only:
    - protected-branch
  environment:
    name: production
    url: https://my-production-url.com
  when: manual
  allow_failure: false

# Secure Variables and Secrets
secrets:
  stage: test
  script:
    - echo "Using secure variable: $SECURE_VARIABLE"
  only:
    - protected-branch
```
#### Explanation
- **Protected Variables**: `SECURE_VARIABLE` and `DOCKER_AUTH` tokens are protected and masked, ensuring they are not exposed in logs.
- **Protected Branches**: Jobs are restricted to protected branches, reducing the risk of unauthorized access.
- **Manual Deployment**: Deployments require manual approval, adding an additional layer of security.
- **Environment-Specific Variables**: Variables are defined and used within specific stages and environments, ensuring that credentials are isolated and managed securely.


## GitLab Insecure System Configuration Protection
GitLab implements several strategies and features to protect against insecure system configuration, ensuring that the platform and its components are configured securely to mitigate potential vulnerabilities. Here are the key protections and mechanisms GitLab employs:
#### 1. **Secure Defaults**
- **Out-of-the-Box Security**: GitLab is designed with secure default configurations for new installations, ensuring that the system is secure from the start.
- **Default Permissions**: Default permissions and access controls are set to minimize the risk of unauthorized access.
#### 2. **Configuration Management**
- **Configuration Files**: GitLab's configuration is managed through structured files, such as `gitlab.rb`, which provide a single point of configuration for the system.
- **Configuration Validation**: GitLab includes tools to validate configuration files, ensuring they are correctly formatted and adhere to best practices.
#### 3. Access Controls
- **Role-Based Access Control (RBAC)**: Granular permissions are assigned based on roles (e.g., Guest, Reporter, Developer, Maintainer, Owner), restricting access to critical system functions and resources.
- **Admin Control**: Administrative actions are restricted to users with appropriate privileges, reducing the risk of accidental or malicious changes.
#### 4. ***Network Security***
- ***IP Allowlisting**: GitLab supports IP allowlisting, which restricts access to the GitLab instance to specified IP addresses or ranges, ensuring that only trusted networks can connect.*
- ***Firewall Rules**: Implement firewall rules to restrict access to GitLab services and ports, reducing the attack surface.*
#### 5. **Secure Communication**
- **TLS/SSL Encryption**: GitLab supports TLS/SSL encryption for secure communication between clients and the server, protecting data in transit.
- **SSH Key Management**: SSH keys are used for secure communication with Git repositories, and key rotation policies can be enforced.
#### 6. **Two-Factor Authentication (2FA)**
- **Enforcing 2FA**: Administrators can enforce 2FA for all users, adding an extra layer of security to user accounts.
- **SSO and External Authentication**: GitLab integrates with Single Sign-On (SSO) providers and external authentication systems (e.g., LDAP, OAuth), ensuring secure and centralized user management.
#### 7. **Audit Logging and Monitoring**
- **Comprehensive Logs**: GitLab maintains detailed logs of user actions, system changes, and access attempts. These logs can be monitored and analyzed to detect and respond to security incidents.
- **Alerting and Notifications**: Configure alerts and notifications for critical events, such as failed login attempts or configuration changes, to ensure timely response to potential security issues.
#### 8. **Automated Security Testing**
- **Static Application Security Testing (SAST)**: GitLab can automatically scan code for vulnerabilities during the CI/CD process.
- **Dynamic Application Security Testing (DAST)**: DAST scans are performed to identify vulnerabilities in running applications.
#### 9. **Dependency and Container Scanning**
- **Dependency Scanning**: GitLab scans for vulnerabilities in project dependencies, ensuring that third-party libraries are secure.
- **Container Scanning**: Docker images used in pipelines are scanned for known vulnerabilities.
#### 10. **Regular Updates and Patching**
- **Automatic Updates**: GitLab can be configured to automatically apply updates and security patches, ensuring the system remains up-to-date with the latest security fixes.
- **Security Advisories**: GitLab provides security advisories and release notes to inform administrators of critical updates and patches.
#### 11. **Environment Isolation**
- **Separate Environments**: Use separate environments for development, testing, staging, and production to isolate potential issues and reduce the impact of configuration errors.
- **Protected Environments**: Define protected environments and restrict access to critical environments, ensuring only authorized users can deploy changes.
#### 12. **Secure Backup and Recovery**
- **Encrypted Backups**: Ensure that backups are encrypted to protect data integrity and confidentiality.
- **Regular Backup Testing**: Regularly test backup and recovery procedures to ensure data can be restored in the event of a failure or security incident.
#### Example of Secure GitLab Configuration
Here’s an example of a secure GitLab configuration in the `gitlab.rb` file:
```ruby
external_url 'https://gitlab.example.com'
nginx['redirect_http_to_https'] = true
nginx['ssl_certificate'] = "/etc/gitlab/ssl/gitlab.example.com.crt"
nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/gitlab.example.com.key"

gitlab_rails['gitlab_shell_ssh_port'] = 2222

# Enforce 2FA for all users
gitlab_rails['omniauth_providers'] = [
  {
    "name" => "saml",
    "label" => "Company SSO",
    "args" => {
      "assertion_consumer_service_url" => "https://gitlab.example.com/users/auth/saml/callback",
      "idp_cert_fingerprint" => "FINGERPRINT",
      "idp_sso_target_url" => "https://idp.example.com/saml/sso",
      "name_identifier_format" => "urn:oasis:names:tc:SAML:2.0:nameid-format:transient"
    }
  }
]

# IP Allowlisting
gitlab_rails['trusted_proxies'] = ['192.168.1.0/24']

# LDAP Authentication
gitlab_rails['ldap_enabled'] = true
gitlab_rails['ldap_servers'] = YAML.load <<-EOS
main:
  label: 'LDAP'
  host: '_your_ldap_server'
  port: 636
  uid: 'sAMAccountName'
  bind_dn: 'CN=binduser,CN=Users,DC=example,DC=com'
  password: '_your_bind_password'
  encryption: 'simple_tls' # "start_tls" or "simple_tls" or "plain"
  verify_certificates: true
  smartcard_auth: false
  active_directory: true
  allow_username_or_email_login: false
  lowercase_usernames: false
  block_auto_created_users: false
  base: 'DC=example,DC=com'
  user_filter: ''
  group_base: ''
  admin_group: ''
  sync_ssh_keys: false
EOS
```
#### Explanation
- **Secure Communication**: Enforces HTTPS and configures SSL certificates.
- **Custom SSH Port**: Changes the default SSH port to enhance security.
- **Enforce 2FA**: Configures SAML-based SSO with enforced 2FA.
- **IP Allowlisting**: Restricts access to trusted IP addresses.
- **LDAP Integration**: Configures LDAP for secure user authentication.


## GitLab Ungoverned Usage of 3rd Party Services Protection
GitLab employs a variety of strategies and features to protect against the ungoverned usage of third-party services. These measures ensure that third-party services and dependencies are used securely and in compliance with organizational policies. Here are the key protections and mechanisms GitLab uses:
#### 1. **Dependency Scanning**
- **Automated Scanning**: GitLab CI/CD pipelines can automatically scan for vulnerabilities in third-party dependencies using Dependency Scanning. This helps identify and remediate known vulnerabilities in libraries and packages used by the project.
- **Reports and Alerts**: Dependency scanning results are provided in detailed reports, and alerts are generated for critical vulnerabilities, allowing teams to take immediate action.
#### 2. ***License Compliance***
- ***License Scanning**: GitLab can automatically scan project dependencies for license information, ensuring that all third-party libraries comply with the organization’s licensing policies.*
- ***License Policies**: Administrators can define approved and prohibited licenses. If a prohibited license is detected, the pipeline can be configured to fail or generate alerts.*
#### 3. Security Dashboards
- **Project Security Dashboard**: Provides a summary of security vulnerabilities detected in the project, including those from third-party services and dependencies.
- **Group Security Dashboard**: Aggregates security information across multiple projects, helping administrators monitor and manage vulnerabilities on a larger scale.
#### 4. ***Approval Workflows***
- ***Merge Request Approvals**: GitLab can enforce approval workflows for merge requests that include changes to third-party dependencies. This ensures that changes are reviewed and approved by authorized team members before being integrated.*
- ***Protected Branches**: Only authorized users can push changes to protected branches, ensuring that updates to third-party services and dependencies are controlled and reviewed.*
#### 5. **Policy Management**
- **Code Quality Rules**: Define and enforce code quality rules that include checks for third-party service usage, ensuring that only approved and secure libraries are used.
- **Custom Policies**: Use custom scripts and policies in CI/CD pipelines to enforce organizational standards for third-party services and dependencies.
#### 6. **Environment-Specific Controls**
- **Environment Variables**: Use environment-specific variables to manage credentials and access to third-party services, ensuring that these are securely handled and not exposed in the codebase.
- **Protected Environments**: Define and protect environments to control who can deploy to critical stages such as production, reducing the risk of ungoverned third-party service usage.
#### 7. **Audit Logging and Monitoring**
- **Comprehensive Logs**: GitLab maintains detailed audit logs of all user activities, including changes to third-party service configurations and dependency updates. These logs help in tracking and investigating unauthorized usage.
- **Monitoring and Alerts**: Set up monitoring and alerts for changes involving third-party services, enabling timely detection and response to potential issues.
#### 8. **Integration with External Security Tools**
- **Security Tool Integration**: GitLab integrates with external security tools and services (e.g., Snyk, Dependabot) to enhance dependency and third-party service security.
- **Continuous Monitoring**: Use external tools for continuous monitoring of third-party dependencies to ensure they remain secure over time.
#### 9. **User Education and Training**
- **Security Awareness**: Promote security awareness among developers regarding the risks associated with third-party services and dependencies.
- **Best Practices**: Encourage adherence to best practices for selecting, integrating, and managing third-party services.
#### Example of a Secure GitLab CI/CD Pipeline Configuration
Here is an example of how to implement some of these security practices in a `.gitlab-ci.yml` file:
```yaml
stages:
  - build
  - test
  - dependency-scanning
  - license-compliance
  - deploy

# Build Stage
build:
  stage: build
  script:
    - docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG .
  only:
    - protected-branch

# Test Stage
test:
  stage: test
  script:
    - echo "Running tests..."
  only:
    - protected-branch

# Dependency Scanning Stage
dependency-scanning:
  stage: dependency-scanning
  image: docker:latest
  services:
    - docker:dind
  script:
    - apt-get update && apt-get install -y dependency-check
    - dependency-check --project "Project Name" --scan ./ --format "XML" --out dependency-check-report.xml
  artifacts:
    reports:
      dependency_scanning: dependency-check-report.xml
  only:
    - protected-branch

# License Compliance Stage
license-compliance:
  stage: license-compliance
  script:
    - license_finder report --format json > license-compliance-report.json
  artifacts:
    reports:
      license_management: license-compliance-report.json
  only:
    - protected-branch

# Deploy Stage
deploy:
  stage: deploy
  script:
    - docker run -d -p 80:80 $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  environment:
    name: production
    url: https://my-production-url.com
  when: manual
  only:
    - protected-branch
```
#### Explanation

- **Stages**: Defines stages (build, test, dependency-scanning, license-compliance, deploy) in the pipeline.
- **Protected Branches**: Ensures that jobs are only executed on protected branches, reducing the risk of unauthorized changes.
- **Dependency Scanning**: Scans for vulnerabilities in third-party dependencies and generates a report.
- **License Compliance**: Checks third-party dependencies for license compliance and generates a report.
- **Manual Deployment**: The deploy job requires manual approval, adding an additional layer of control over the deployment process.


## GitLab Improper Artifact Integrity Validation Protection
GitLab employs several mechanisms to protect against improper artifact integrity validation, ensuring that artifacts produced and used within CI/CD pipelines are secure, untampered, and verified. Here are the key protections and mechanisms GitLab uses:
#### 1. ***Artifact Integrity Checks***
- ***Checksum Verification**: GitLab can generate checksums for artifacts, allowing integrity verification by comparing the generated checksum with the expected value.*
- ***Artifact Metadata**: GitLab stores metadata for each artifact, including file size and hash, which can be used to verify the integrity of the artifacts.*
#### 2. ***Pipeline Security Policies***
- ***Job Definitions**: Ensure that jobs producing and consuming artifacts are defined within the same pipeline or are explicitly trusted. This reduces the risk of artifacts being tampered with across different pipelines.*
- ***Secure Storage**: Artifacts are stored in a secure, versioned storage system, ensuring that only authorized users and jobs can access and modify them.*
#### 3. Role-Based Access Control (RBAC)
- **Granular Permissions**: Use RBAC to control who can create, access, and modify artifacts. Permissions are assigned based on roles such as Guest, Reporter, Developer, Maintainer, and Owner.
- **Protected Environments**: Define and enforce access controls for protected environments, ensuring that only authorized users can deploy or promote artifacts to these environments.
#### 4. ***Artifact Signing***
- ***Digital Signatures**: GitLab supports signing artifacts with digital signatures to ensure their authenticity and integrity. Signed artifacts can be verified during deployment to ensure they have not been tampered with.*
- ***GPG Signing**: GitLab CI/CD supports GPG signing of commits and tags, providing a way to verify the authenticity and integrity of the code and artifacts.*
#### 5. **Secure CI/CD Pipelines**
- **Isolated Runners**: Use isolated CI/CD runners to execute pipeline jobs, ensuring that the build environment is secure and cannot be tampered with by unauthorized users.
- **Environment Variables**: Use environment variables to manage sensitive data securely within pipelines. Protect these variables to ensure they are only available to jobs running on protected branches or tags.
#### 6. **Audit Logging and Monitoring**
- **Comprehensive Logs**: GitLab maintains detailed audit logs of artifact creation, access, and modifications. These logs can be reviewed to detect and investigate any suspicious activities.
- **Monitoring and Alerts**: Set up monitoring and alerts for activities related to artifacts, such as unauthorized access or modification attempts, enabling timely detection and response.
#### 7. **Immutable Artifacts**
- **Immutability Policies**: Enforce immutability policies on artifacts to ensure they cannot be modified after creation. This prevents tampering and ensures the integrity of the artifacts over time.
- **Versioning**: Use versioning to keep track of different versions of artifacts, ensuring that each version can be independently verified and audited.
#### 8. **Dependency and Container Scanning**
- **Dependency Scanning**: Automatically scan dependencies for known vulnerabilities, ensuring that the artifacts built from these dependencies are secure.
- **Container Scanning**: Scan Docker images used in CI/CD pipelines for vulnerabilities to ensure that containerized artifacts are secure.
#### 9. **Artifact Management**
- **Artifact Retention Policies**: Define retention policies for artifacts to manage their lifecycle and ensure that old or unused artifacts are appropriately archived or deleted.
- **Artifact Provenance**: Track the provenance of artifacts, including their source, build environment, and any transformations applied, to ensure their integrity and authenticity.
#### Example of a Secure GitLab CI/CD Pipeline Configuration
Here is an example of a `.gitlab-ci.yml` configuration that demonstrates secure handling and validation of artifacts:
```yaml
stages:
  - build
  - test
  - verify
  - deploy

variables:
  IMAGE_TAG: "latest"
  SECURE_VARIABLE: $CI_JOB_TOKEN

# Build Stage
build:
  stage: build
  script:
    - docker build -t $CI_REGISTRY_IMAGE:$IMAGE_TAG .
    - echo "Building artifact..."
    - mkdir -p artifacts
    - echo "Sample artifact content" > artifacts/sample.txt
    - sha256sum artifacts/sample.txt > artifacts/sample.txt.sha256
  artifacts:
    paths:
      - artifacts/
  only:
    - protected-branch

# Test Stage
test:
  stage: test
  script:
    - echo "Running tests..."
    - sha256sum -c artifacts/sample.txt.sha256
  only:
    - protected-branch

# Verify Stage
verify:
  stage: verify
  script:
    - echo "Verifying artifact integrity..."
    - sha256sum -c artifacts/sample.txt.sha256
  only:
    - protected-branch

# Deploy Stage
deploy:
  stage: deploy
  script:
    - echo "Deploying artifact..."
    - docker run -d -p 80:80 $CI_REGISTRY_IMAGE:$IMAGE_TAG
  environment:
    name: production
    url: https://my-production-url.com
  when: manual
  only:
    - protected-branch
```
#### Explanation
- **Artifact Creation and Hashing**: The build stage creates an artifact and generates a SHA-256 checksum for it, storing both the artifact and its checksum.
- **Checksum Verification**: Both the test and verify stages validate the artifact's integrity by checking its checksum against the stored value.
- **Protected Branches**: Jobs are restricted to protected branches, ensuring that only authorized changes are processed.
- **Manual Deployment**: The deploy job requires manual approval, adding an additional layer of control over the deployment process.

## GitLab Insufficient Logging and Visibility Protection
GitLab employs various strategies and features to protect against insufficient logging and visibility, ensuring that activities within the platform are adequately monitored and auditable. Here are the key protections and mechanisms GitLab uses:
#### 1. **Comprehensive Audit Logs**
- **User Activity Logs**: GitLab maintains detailed logs of user activities, including login attempts, project access, repository changes, and CI/CD pipeline executions. These logs provide a comprehensive view of user interactions within the platform.
- **Admin Activity Logs**: Activities performed by administrators, such as changes to system settings and user management actions, are also logged. This helps in auditing administrative actions.
#### 2. **CI/CD Pipeline Logs**
- **Job Logs**: Each job within a CI/CD pipeline generates detailed logs that capture the execution steps, including command outputs, errors, and statuses. These logs are accessible through the GitLab UI and provide visibility into pipeline execution.
- **Artifact Logs**: Logs related to the creation, modification, and deletion of artifacts are maintained, ensuring that all changes to artifacts are tracked.
#### 3. **System and Application Logs**
- **System Logs**: GitLab generates logs for system-level events, including server operations, background jobs, and service interactions. These logs help in monitoring the overall health and performance of the GitLab instance.
- **Application Logs**: Logs specific to GitLab applications, such as web application requests and responses, are maintained. These logs help in diagnosing application-level issues.
#### 4. **Audit Events**
- **Custom Audit Events**: GitLab allows the creation of custom audit events for specific actions or workflows, providing additional logging capabilities tailored to organizational needs.
- **Audit Event API**: GitLab provides an API to retrieve audit events, enabling integration with external monitoring and logging tools.
#### 5. **Access Controls and Visibility**
- **Role-Based Access Control (RBAC)**: GitLab uses RBAC to control access to logs and audit trails, ensuring that only authorized users can view or modify sensitive logs.
- **Protected Logs**: Certain logs can be protected to restrict access to high-privilege users, ensuring that critical logs are not tampered with or accessed by unauthorized users.
#### 6. **Log Retention Policies**
- **Configurable Retention**: GitLab allows administrators to configure log retention policies, ensuring that logs are retained for the required duration based on regulatory and organizational policies.
- **Archiving Logs**: Older logs can be archived to preserve historical data while maintaining system performance.
#### 7. **Integration with External Tools**
- **Logging Services**: GitLab can integrate with external logging services such as Elasticsearch, Splunk, and other SIEM (Security Information and Event Management) tools. This enables centralized logging and advanced analysis capabilities.
- **Monitoring and Alerting**: Integration with monitoring tools like Prometheus and Grafana allows for real-time monitoring and alerting based on log data, helping to quickly detect and respond to anomalies.
#### 8. **Real-Time Monitoring**
- **Built-In Monitoring**: GitLab provides built-in monitoring features for tracking the health and performance of CI/CD pipelines, including metrics on job durations, success rates, and resource usage.
- **Dashboards**: Customizable dashboards provide a visual overview of key metrics and logs, helping administrators and developers to monitor system status and performance at a glance.
#### 9. **Security Dashboards**
- **Project Security Dashboard**: Displays security-related information for a specific project, including vulnerabilities detected in code and dependencies.
- **Group Security Dashboard**: Provides an aggregated view of security metrics across multiple projects within a group, helping to manage security at an organizational level.
#### Example of Enhanced Logging in GitLab CI/CD Pipeline
Here is an example of how to enhance logging and visibility within a `.gitlab-ci.yml` file:
```yaml
stages:
  - build
  - test
  - deploy

variables:
  IMAGE_TAG: "latest"
  SECURE_VARIABLE: $CI_JOB_TOKEN

# Build Stage
build:
  stage: build
  script:
    - echo "Starting build stage"
    - docker build -t $CI_REGISTRY_IMAGE:$IMAGE_TAG .
    - echo "Build stage completed"
  artifacts:
    paths:
      - build_logs/
  after_script:
    - echo "Build logs:"
    - cat build_logs/*
  only:
    - protected-branch

# Test Stage
test:
  stage: test
  script:
    - echo "Starting test stage"
    - echo "Running tests..."
    - echo "Test stage completed"
  artifacts:
    paths:
      - test_logs/
  after_script:
    - echo "Test logs:"
    - cat test_logs/*
  only:
    - protected-branch

# Deploy Stage
deploy:
  stage: deploy
  script:
    - echo "Starting deploy stage"
    - docker run -d -p 80:80 $CI_REGISTRY_IMAGE:$IMAGE_TAG
    - echo "Deploy stage completed"
  environment:
    name: production
    url: https://my-production-url.com
  when: manual
  after_script:
    - echo "Deployment logs:"
    - docker logs $(docker ps -lq)
  only:
    - protected-branch
```
#### Explanation
- **Detailed Logs**: Each stage includes echo statements to log the start and end of the stage, providing better visibility into the pipeline execution.
- **Artifacts**: Logs are collected and stored as artifacts, ensuring they are available for review after the pipeline completes.
- **After Script**: The `after_script` section in each stage captures and displays logs, providing immediate feedback on the execution status and output.


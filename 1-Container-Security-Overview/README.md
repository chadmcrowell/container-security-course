# Container Security with Kubernetes & GitLab CI/CD

![Container Security Course Title Slide](../../0-Introduction-and-Setup/img/00-SLIDES-Container-Security-with-Kubernetes-and-Gitlab_HELLO-AND-WELCOME_05092024.png)

## SECTION 1: Container Security Overview

- LESSON 0: [Common Threats & The Importance of Container Security](Common-threats.md)
- LESSON 1: [Hardening The Host Operating System](Hardening-the-host.md)
- LESSON 2: [Container Runtime Security](Container-runtime-security.md)
- LESSON 3: [Secure Network Communication to and from Containers](Secure-network-comms.md)
- LESSON 4: [GitLab Tools for Container Security](Gitlab-tools.md)
# Common Threats & The Importance of Container Security

![Common Threats Title Slide](img/06-SLIDES-Container-Security-with-Kubernetes-and-Gitlab_COMMON-THREATS_05312024.001.png)

## Container Security Benefits
- **Isolation:** Containers provide process and resource isolation, meaning that each container runs in its own isolated environment. This limits the impact of a vulnerability in one container affecting others or the host system. 
- **Immutability:** Containers are often built from immutable images, ensuring consistency across different environments. This reduces the risk of configuration drift, which can introduce vulnerabilities.
- **Minimal**: Containers typically include only the necessary components required for an application to run, reducing the attack surface compared to traditional virtual machines that may have more extensive and potentially vulnerable software.

## Vulnerabilities
- **Outdated or Unpatched Software:** Containers often include operating system libraries and application dependencies. The application code within the container may be out of date and have vulnerabilities, such as buffer overflows, SQL injection, or cross-site scripting (XSS) flaws. If these are not regularly updated or patched, they can become vulnerable to known security flaws. 
- **Misconfigured Containers:** Improper configuration can lead to issues like excessive privileges, inadequate network isolation, or exposed sensitive information. Containers without defined resource limits can lead to Denial of Service (DoS) attacks if they consume excessive resources.
- **Insecure Base Images:** Using base images that have known vulnerabilities can propagate these issues to all derived images. [Snyk pointing out vulnerable images](https://snyk.io/blog/top-ten-most-popular-docker-images-each-contain-at-least-30-vulnerabilities/), [Leaky vessels impacting tools like Docker's Runc and BuildKit](https://snyk.io/blog/leaky-vessels-docker-runc-container-breakout-vulnerabilities/). Using container images from unverified sources can introduce malicious code or backdoors.
- **Embedded Secrets:** Including hard-coded secrets, such as API keys or passwords, within container images can lead to unauthorized access if these images are compromised.

[Leaky Vessels](https://snyk.io/blog/leaky-vessels-docker-runc-container-breakout-vulnerabilities/) : four vulnerabilities in core container infrastructure components that allow container escapes. **An attacker could use these container escapes to gain unauthorized access to the underlying host operating system from within the container**

## Recent Data Breaches
- https://www.electric.ai/blog/recent-big-company-data-breaches
- https://tech.co/news/data-breaches-updated-list

These examples highlight the importance of promptly addressing vulnerabilities to protect sensitive data, maintain customer trust, and avoid financial and reputational damage. By ensuring robust security measures and timely patching of known vulnerabilities, companies can significantly reduce the risk of cyberattacks and safeguard their assets and reputation.

JUST IN 2024:
**TicketMaster Data Breach:** June 2024 leaked Over 560 million customer order history & personal information leaked.

**American Express Data Leak:** Leaked customer data in March 2024 from unauthorized access to a third-party merchant processor.

**Change Healthcare Ransomware Attack:** hit by ransomware attack in February 2024. 

**Fujitsu Hacked:** In March 2024, Fujitsu confirmed the presence of malware on its corporate network, which may have left customer information vulnerable to hackers

**Roku Cyber Attack:** March 2024, experienced a cyber attack affecting 15,000 account holders. Roku claimed hackers had “likely obtained certain usernames and passwords of consumers from third-party sources. 

**U-haul data breach:**  Feb 2024, U-Haul began informing 67,000 customers of a data breach

**Mother of all breaches:** Jan 2024, a colossal 12 terabytes of information and 26 billion records leaked, sensitive information spanning various sources like Tencent, Weibo, Twitter, MySpace, Wattpad, LinkedIn, Adobe, Canva, MyFitnessPal, and government sites like Alabama.gov.


## Detect and Avoid Vulnerabilities

-	**Vulnerability Search:** Allows users to search for vulnerabilities by various criteria such as keyword, severity, and CVE identifier.
-	**Data Feeds:** Provides automated data feeds that can be integrated into security tools and processes.
-	**Analysis:** Offers analysis and metrics on the impact and prevalence of vulnerabilities.
-	**Security Advisories:** Includes security advisories and references to patches and updates for vulnerable software.

The NVD is an essential resource for cybersecurity professionals, software developers, and IT administrators looking to stay informed about vulnerabilities and enhance the security of their systems.

Staying ahead of common vulnerabilities is crucial for companies to protect their systems and data. Here are some best practices that companies typically employ:
1. **Regular Updates and Patching**
	•	Timely Patching: Regularly apply patches and updates to software, operating systems, and applications to close known vulnerabilities. Many vulnerabilities are discovered and patched by vendors, so keeping software up to date is essential ￼ ￼.
	•	Automated Updates: Use automated patch management tools to ensure patches are applied promptly and consistently across all systems.
2. **Vulnerability Scanning and Management**
	•	Continuous Scanning: Implement continuous vulnerability scanning using tools such as Aqua, Trivy, Sysdig, & Snyk. These tools can help identify and report vulnerabilities in real time.
	•	Regular Audits: Conduct regular security audits and penetration testing to uncover vulnerabilities that may not be detected by automated tools.
3. **Adopting Security Frameworks and Standards**
	•	Security Frameworks: Implement security frameworks such as NIST, ISO 27001, and CIS Controls. These frameworks provide comprehensive guidelines and best practices for securing systems and data.
4. **Secure Software Development Lifecycle (SDLC)**
	•	Integrate Security in SDLC: Integrate security practices into the software development lifecycle, including threat modeling, code review, and security testing during the development process.
	•	DevSecOps: Adopt DevSecOps practices to incorporate security into the development and operations processes, ensuring continuous security throughout the software lifecycle.
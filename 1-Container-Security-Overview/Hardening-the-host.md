# Hardening The Host Operating System

![Hardening the host title slide](img/07-SLIDES-Container-Security-with-Kubernetes-and-Gitlab_HARDENING-THE-HOST_06142024.001.png)

## Container Host (Node)
- **Node OS:** OS must be tailored to containers
- **Filesystem:** The root filesystem must be read-onlly
- **Overconsume:** Limit CPU/Mem/IO to not overconsume and bring down the node
- **Logs:** Collect Logs for auditing and incident response

## Container OS
- **READ-ONLY:** immutable infra, consistent state across reboots, less maintenance
- **UPDATES:** auto-update, usually by replacing the entire filesystem image
- **MINIMAL:** reduce attack surface
- **COMPATIBLE:** integration with Kubernetes (OS includes containerd, supports CNI, CSI, supports isolation, etc.)

## What we get with Kubernetes
- **Node Authorization**: Ensures that kubelets can only access resources specific to their node, enhancing security by limiting the scope of node-level operations
- **API Server Authentication**: Kubernetes API server supports multiple authentication mechanisms such as client certificates, bearer tokens, OpenID Connect, and Webhook token authentication. 
- **Audit Logging**: Kubernetes audit logs capture all requests to the API server, providing a detailed record of all activities within the cluster for security auditing and incident response.
- **Admission Controllers:** intercept requests to the k8s-api before an object is persisted (allowing you to enforce security policies, validate configurations, and mutate resources)
- **Resource Quotas and Limits**: Ensures that resources are allocated fairly among users and prevents any single pod or namespace from consuming excessive resources.


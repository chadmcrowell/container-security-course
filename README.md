# CONTAINER SECURITY WITH KUBERNETES & GITLAB CI/CD
![Container Security Course Banner](0-Introduction-and-Setup/img/00-SLIDES-Container-Security-with-Kubernetes-and-Gitlab_HELLO-AND-WELCOME_05092024.png)

This course is designed to help you build containers, check for vulnerabilities and deploy to a Kubernetes cluster, all using GitLab CI/CD. 

The course will cover the security features of GitLab, and also discuss best practices for securing the Kubernetes cluster once the app is deployed and running.

You will be provided with applications to use for deployments, so you do not need to be well versed in any programming languages.

In this course, we'll be using the self-managed version of GitLab, as well as bootstrapping a Kubernetes cluster using Kubeadm.

This course includes many tutorials and walkthroughs, where you are encouraged to follow along and get hands-on experience with implementing these container security best practices.

## COURSE OUTLINE
- [SECTION 0: Introduction and Installation](0-Introduction-and-Setup/README.md)
- [SECTION 1: Container Security Overview](1-Container-Security-Overview/README.md)
